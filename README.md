# complaintronix

## A flutter application to register and manage student complaints related to their internet connections in GIKI hostels

- The backend API was built using Express.js
  - The repo for backend is [here](https://github.com/MusaGillani/complaintronix)

